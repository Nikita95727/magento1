<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class AbsoluteWeb_News_Model_News extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        parent::_construct();
        $this->_init('absoluteweb_news/news');
    }

    /**
     * Function sets the current date to new item
     */
    protected function _setCreateDate()
    {
        $data = array(
          'create_date' => date('Y-m-d H:i:s')
        );
        $this->addData($data);
        return $this;
    }

    /**
     * Function updates the date of item
     */
    protected function _setUpdateDate()
    {
        $data = array(
            'update_date' => date('Y-m-d H:i:s')
        );
        $this->addData($data);
        return $this;
    }

    /**
     * @return Mage_Core_Model_Abstract|void
     */
    protected function _beforeSave()
    {
        if ($this->isObjectNew()) {
            $this->_setCreateDate();
        }
        $this->_setUpdateDate();
        if (!Zend_Validate::is($this->getTitle(),'NotEmpty')) {
            Mage::throwException(Mage::helper('absoluteweb_news')->__('Title should not be empty'));
        }
        if (!Zend_Validate::is($this->getImage(),'NotEmpty')) {
            Mage::throwException(Mage::helper('absoluteweb_news')->__('Image should be entered'));
        }
        if (!Zend_Validate::is($this->getContent(),'NotEmpty')) {
            Mage::throwException(Mage::helper('absoluteweb_news')->__('The content should be entered'));
        }
        if (!Zend_Validate::is($this->getPublish(),'NotEmpty')) {
            Mage::throwException(Mage::helper('absoluteweb_news')->__('the pusblish should be marked'));
        }
        return parent::_beforeSave();
    }
}

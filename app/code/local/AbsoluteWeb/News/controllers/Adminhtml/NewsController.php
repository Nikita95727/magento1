<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class AbsoluteWeb_News_Adminhtml_NewsController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('absoluteweb_news');
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_initNews();
        $this->loadLayout();
        $this->_addContent(
            $this->getLayout()->createBlock('absoluteweb_news/adminhtml_news_edit',  'edit'),
            $this->getLayout()->createBlock('absoluteweb_news/adminhtml_news_edit_form',  'new')
        );
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->_setActiveMenu('absoluteweb_news');
        $this->renderLayout();
    }

    /**
     *  The method edits the current news
     */
    public function editAction()
    {
        $this->_forward('new');
    }

    /**
     * The method saves an edit or new action
     */
    public function saveAction()
    {
        $newsObject = Mage::getModel('absoluteweb_news/news');
        $id = $this->getRequest()->getParam('entity_id', null);
        try{
            if ((!intval($id)) && ($id!=null)) {
                $this->_setErrorMessage('invalid id');
                $this->_redirect('*/*/index');
                return;
            }

            $data = $this->getRequest()->getParams();

            if ($id){
                $newsObject = $newsObject->load($id);
            }

            $newsObject->addData($data)->save();
            $msg = $this->__('The item had been edited');

            if ($newsObject->isObjectNew()){
                $msg = $this->__('The new item had been added');
            }
            $this->_setSuccessMessage($msg);
            $this->_redirect('*/*/index');
        } catch (Mage_Core_Exception $e){
            $this->_setErrorMessage($e->getMessage());
            $this->_redirect('*/*/index');
        } catch (Exception $e){
            $this->_setErrorMessage("something is wrong");
            Mage::log($e->getMessage(), 1);
            $this->_redirect('*/*/index');
        }
    }

    /**
     * The method deletes the items one by one.
     */
    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id');
        if ((!intval($id)) && ($id!=null)){
            $this->_setErrorMessage('invalid id');
            $this->_redirect('*/*/index');
            return;
        }
        try {
            $this->_deleteCurrentItem($id);
            $this->_setSuccessMessage( 'Total of %d record(s) were successfully deleted');
        } catch (Exception $e){
            $this->_setErrorMessage($e->getMessage());
        }
        $this->_redirect('*/*/index');
    }

    /**
     * The method deletes the array of items.
     */
    public function deleteMassAction()
    {
        $requestIds = $this->getRequest()->getParam('id');

        try {
            if (!is_array($requestIds)) {
                $this->_setErrorMessage('Please select request(s)');
                $this->_redirect('*/*/index');
            }
            foreach ($requestIds as $reqtId) {
                $this->_deleteCurrentItem($reqtId);
            }
            $this->_setSuccessMessage( 'Your records successfully deleted');
        } catch (Exception $e) {
            $this->_setErrorMessage($e->getMessage());
        }

        $this->_redirect('*/*/index');
    }
    /**
     * Init actions
     */
    protected function _initNews()
    {
        $helper = Mage::helper('absoluteweb_news');
        $this->_title($helper->__('AbsoluteWeb'))->_title($helper->__('News'));
        $model = Mage::getModel('absoluteweb_news/news');
        $newsId = $this->getRequest()->getParam('id');
        if (!is_null($newsId)) {
            $model->load($newsId);
        }
        Mage::register('current_news', $model);
    }

    private function _deleteCurrentItem($id)
    {
        Mage::getModel('absoluteweb_news/news')->load($id)->delete();
    }

    /**
     * @msg - the text of message
     * @return the message about successful adding/editing
     */
    private function _setSuccessMessage($msg)
    {
        return Mage::getSingleton('core/session')
            ->addSuccess($this->__($msg));
    }

    /**
     * @msg - the text of message
     * @return the message about error adding/editing
     */
    private function _setErrorMessage($msg)
    {
        return Mage::getSingleton('core/session')
            ->addError($this->__($msg));
    }
}
